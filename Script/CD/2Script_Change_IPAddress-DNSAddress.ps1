﻿$IPAddress = "172.31.0.1"
$Prefix = "24"
$Gateway = "172.31.0.254"
$IPAddressDNS = "172.31.0.1"

New-NetIPAddress -IPAddress $IPAddress -PrefixLength $Prefix -InterfaceIndex (Get-NetAdapter).ifIndex -DefaultGateway $Gateway
Set-DnsClientServerAddress -InterfaceIndex (Get-NetAdapter).ifIndex -ServerAddresses ($IPAddressDNS)