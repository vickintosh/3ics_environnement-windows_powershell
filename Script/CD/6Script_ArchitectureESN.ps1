#Variable
$FQDN="ESN.dom"
$DomainName="ESN"
$TLD="dom"
$firstOU="Sites"
$GrpOU="Group"
$sites=('Lyon','Paris')
$services=('Informatique','Marketing','Direction','Comptabilité','Production')
$servicesM=('Informatique','Marketing','Direction','Comptabilité','Production','Materiels')
$personnel=('Employé','Responsable')
$materiels=('Ordinateurs','Serveurs','Imprimantes','Mobiles')

$GrpGlobalPath = "OU=Global,OU="+$GrpOU+",DC="+$DomainName+",DC="+$TLD
$GrpDLPath = "OU=DomainLocal,OU="+$GrpOU+",DC="+$DomainName+",DC="+$TLD
$GrpPersonnel = "G_Personnel"
$GrpResponsable = "G_Responsable"
$GrpEmploye = "G_Employé"
$GrpE1 = "G_First_Employé"
$GrpE2 = "G_Second_Employé"
$TotalR="DL_Total_Ressources"
$RefusR="DL_Refuser_Ressources"
$LectureR="DL_Lecture_Ressources"


$FilePathTemplate = "C:\Users\Administrator\Documents\Template.docx"
[byte[]]$horaire = @(0,0,128,255,255,255,255,255,255,255,255,255,255,255,255,255,255,127,0,0,0)
$nbpersonnel=0
$i=0

#Initialisation
Install-Module PSWriteWord 
Import-Module PSWriteWord

#Function
function New-Password
{
    $Alphabets = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z'
    $numbers = 0..9
    $specialCharacters = '~,!,@,#,$,%,^,&,*,(,),>,<,?,\,/,_,-,=,+'
    $array = @()
    $array += $Alphabets.Split(',') | Get-Random -Count 4
    $array[0] = $array[0].ToUpper()
    $array[-1] = $array[-1].ToUpper()
    $array += $numbers | Get-Random -Count 3
    $array += $specialCharacters.Split(',') | Get-Random -Count 3
    ($array | Get-Random -Count $array.Count) -join ""
}

function New-RandomUser {
    <#
        .SYNOPSIS
            Generate random user data from Https://randomuser.me/.
        .DESCRIPTION
            This function uses the free API for generating random user data from https://randomuser.me/
        .EXAMPLE
            Get-RandomUser 10
        .EXAMPLE
            Get-RandomUser -Amount 25 -Nationality us,gb 
        .LINK
            https://randomuser.me/
    #>
    [CmdletBinding()]
    param (
        [Parameter(Position = 0)]
        [ValidateRange(1,500)]
        [int] $Amount,

        [Parameter()]
        [ValidateSet('Male','Female')]
        [string] $Gender,

        # Supported nationalities: AU, BR, CA, CH, DE, DK, ES, FI, FR, GB, IE, IR, NL, NZ, TR, US
        [Parameter()]
        [string[]] $Nationality,

        [Parameter()]
        [ValidateSet('json','csv','xml')]
        [string] $Format = 'json',

        # Fields to include in the results.
        # Supported values: gender, name, location, email, login, registered, dob, phone, cell, id, picture, nat
        [Parameter()]
        [string[]] $IncludeFields,

        # Fields to exclude from the the results.
        # Supported values: gender, name, location, email, login, registered, dob, phone, cell, id, picture, nat
        [Parameter()]
        [string[]] $ExcludeFields
    )

    $rootUrl = "http://api.randomuser.me/?format=$($Format)"

    if ($Amount) {
        $rootUrl += "&results=$($Amount)"
    }

    if ($Gender) {
        $rootUrl += "&gender=$($Gender)"
    }

    if ($Nationality) {
        $rootUrl += "&nat=$($Nationality -join ',')"
    }

    if ($IncludeFields) {
        $rootUrl += "&inc=$($IncludeFields -join ',')"
    }

    if ($ExcludeFields) {
        $rootUrl += "&exc=$($ExcludeFields -join ',')"
    }
    
    Invoke-RestMethod -Uri $rootUrl
}
#FirstOU

New-ADOrganizationalUnit -Name $firstOU -Path "DC=$DomainName,DC=$TLD" -ProtectedFromAccidentalDeletion  $false
New-ADOrganizationalUnit -Name $GrpOU -Path "DC=$DomainName,DC=$TLD" -ProtectedFromAccidentalDeletion  $false
New-ADOrganizationalUnit -Name "Global" -Path "OU=$GrpOU,DC=$DomainName,DC=$TLD" -ProtectedFromAccidentalDeletion  $false
New-ADOrganizationalUnit -Name "DomainLocal" -Path "OU=$GrpOU,DC=$DomainName,DC=$TLD" -ProtectedFromAccidentalDeletion  $false

#First groups 
New-ADGroup -Name $GrpE1 -SamAccountName $GrpE1 -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Employé numéro 1 de chaque service"
New-ADGroup -Name $GrpE2 -SamAccountName $GrpE2 -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Employé numéro 2 de chaque service"
New-ADGroup -Name $GrpResponsable -SamAccountName $GrpResponsable -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Responsable de l'entreprise" 
New-ADGroup -Name $GrpEmploye -SamAccountName $GrpEmploye -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Employé de l'entreprise" 
New-ADGroup -Name $GrpPersonnel -SamAccountName $GrpPersonnel -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Personnel de l'entreprise"
New-ADGroup -Name $TotalR -SamAccountName $TotalR -GroupCategory Security -GroupScope DomainLocal -Path $GrpDLPath -Description "Controle Total de Ressources"
New-ADGroup -Name $RefusR -SamAccountName $RefusR -GroupCategory Security -GroupScope DomainLocal -Path $GrpDLPath -Description "Aucune authorisation sur Ressources"
New-ADGroup -Name $LectureR -SamAccountName $LectureR -GroupCategory Security -GroupScope DomainLocal -Path $GrpDLPath -Description "Authorisation de lecture sur Ressources"


Add-ADGroupMember -Identity $GrpPersonnel -Members $GrpResponsable,$GrpEmploye

#Creation all OU and Group
foreach ($ser in $services) {
    New-ADGroup -Name "G_$ser" -SamAccountName "G_$ser" -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Personnel du service $ser"
    New-ADGroup -Name "G_Employé_$ser" -SamAccountName "G_Employé_$ser" -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Employé du service $ser"
    New-ADGroup -Name "G_Responsable_$ser" -SamAccountName "G_Responsable_$ser" -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Responsable du service $ser"
}

foreach ($s in $sites) {
    New-ADOrganizationalUnit -Name $s -Path "OU=$firstOU,DC=$DomainName,DC=$TLD" -ProtectedFromAccidentalDeletion  $false
    New-ADGroup -Name "G_$s" -SamAccountName "G_$s" -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Personnel de $s"
    New-ADGroup -Name "G_Employé_$s" -SamAccountName "G_Employé_$s" -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Employé de $s"
    New-ADGroup -Name "G_Responsable_$s" -SamAccountName "G_Responsable_$s" -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Responsable de $s"

    foreach ($ser in $servicesM) {
        New-ADOrganizationalUnit -Name $ser -Path "OU=$s,OU=$firstOU,DC=$DomainName,DC=$TLD" -ProtectedFromAccidentalDeletion  $false
        if ($ser -eq "Materiels" ) {
            foreach ($mat in $materiels) {
                New-ADOrganizationalUnit -Name $mat -Path "OU=$ser,OU=$s,OU=$firstOU,DC=$DomainName,DC=$TLD" -ProtectedFromAccidentalDeletion  $false
            }
        }
        else {
                New-ADGroup -Name "G_$ser`_$s" -SamAccountName "G_$ser`_$s" -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "Personnel du service $ser de $s"
                
            foreach ($h in $personnel) {
                $GrpList=("G_$h`_$ser","G_$h`_$s","G_$ser`_$s","G_$h","G_$s","G_$ser")
                $GrpName="G_"+$h+"_"+$ser+"_"+$s
                New-ADGroup -Name $GrpName -SamAccountName $GrpName -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath -Description "$h du service $ser de $s"

                foreach ($Grp in $GrpList) {
                    #New-ADGroup -Name $Grp -SamAccountName $Grp -GroupCategory Security -GroupScope Global -Path $GrpGlobalPath #-Description "$h du service $ser de $s"
                    Add-ADGroupMember -Identity $Grp -Members $GrpName
                }

                if ($h -eq "Employé" ) {
                    $nbpersonnel=28
                    $i=0
                }
                elseif ($h -eq "Responsable" ) {
                    $nbpersonnel=2
                    $i=0
                }

                New-ADOrganizationalUnit -Name $h -Path "OU=$ser,OU=$s,OU=$firstOU,DC=$DomainName,DC=$TLD" -ProtectedFromAccidentalDeletion  $false
                Write-Host "Creation of : $s - $ser - $h"

                #Creation User
                while ($i -lt $nbpersonnel) {
                    Write-Host "Creating $i of $nbpersonnel user in $ser of $s"
                    $API = New-RandomUser -Amount 1 -Nationality fr -IncludeFields name,dob,phone,cell -ExcludeFields picture | Select-Object -ExpandProperty results
                    foreach ($user in $API) 
                    {
                        #New Password
                        $userPassword = New-Password

                        $newUserProperties = @{
                            Name = "$($user.name.first) $($user.name.last)"
                            City = "$s"
                            GivenName = $user.name.first
                            Surname = $user.name.last
                            Path = "OU=$h,OU=$ser,OU=$s,OU=$firstOU,DC=$DomainName,DC=$TLD"
                            title = "$h"
                            department="$ser"
                            OfficePhone = $user.phone
                            MobilePhone = $user.cell
                            Company="$DomainName"
                            EmailAddress=$($user.name.first).ToLower()+"."+$($user.name.last).ToLower()+"@"+$($FQDN).ToLower()
                            AccountPassword = (ConvertTo-SecureString $userPassword -AsPlainText -Force)
                            SamAccountName = $($user.name.first).Substring(0,1).ToLower()+"."+$($user.name.last).ToLower()
                            UserPrincipalName = $($user.name.first).Substring(0,1).ToLower()+$($user.name.last).ToLower()+"@"+$($FQDN).ToLower()
                            Enabled = $true
                            CannotChangePassword = $true
                        }
                        
                        if(!(Test-Path -Path "C:\Welcom\$s\$ser\$h"))
                        {
                            New-Item -Path "C:\Welcom\$s\$ser\$h" -ItemType Directory | Out-Null
                        }

                        $WordDocument = Get-WordDocument -FilePath $FilePathTemplate

                        $FilePathInvoice  = "C:\Welcom\$s\$ser\$h\$($user.name.last)-$($user.name.first).docx"
                        Add-WordText -WordDocument $WordDocument -Text 'Creation de Compte' -FontSize 15 -HeadingType  Heading1 -FontFamily 'Arial' -Italic $true | Out-Null


                        Add-WordText -WordDocument $WordDocument -Text 'Voici les informations qui vous permettrons de vous connecter au Domaine Active Directory', " $FQDN" `
                        -FontSize 12, 13 `
                        -Color  Black, Blue `
                        -Bold  $false, $true `
                        -SpacingBefore 15 `
                        -Supress $True

                        Add-WordText -WordDocument $WordDocument -Text 'Login : ', "$($newUserProperties.SamAccountName)" `
                        -FontSize 12, 10 `
                        -Color  Black, Blue `
                        -Bold  $false, $true `
                        -Supress $True

                        Add-WordText -WordDocument $WordDocument -Text 'Mot de passe : ',"$userPassword" `
                        -FontSize 12, 10 `
                        -Color  Black, Blue `
                        -Bold  $false, $true `
                        -Supress $True

                        Add-WordText -WordDocument $WordDocument -Text 'Adresse de messagerie : ',"$($newUserProperties.EmailAddress)" `
                        -FontSize 12, 10 `
                        -Color  Black, Blue `
                        -Bold  $false, $true `
                        -SpacingAfter 15 `
                        -Supress $True

                        Add-WordText -WordDocument $WordDocument -Text "Le Service Informatique." `
                        -FontSize 12 `
                        -Supress $True

                        Save-WordDocument -WordDocument $WordDocument -FilePath $FilePathInvoice -Supress $true  -Language 'fr-FR'
                        try {
                            Write-Host $($newUserProperties.SamAccountName)
                            New-ADUser @newUserProperties
                            Add-ADGroupMember -Identity $GrpName -Members $($newUserProperties.SamAccountName)
                            if ($h -eq "Employé" ) {
                                Set-ADUser -Identity $newUserProperties.SamAccountName -Replace @{logonHours=$horaire}
                                switch ($i) {
                                    0 { Add-ADGroupMember -Identity $GrpE1 -Members $($newUserProperties.SamAccountName)}
                                    1 { Add-ADGroupMember -Identity $GrpE2 -Members $($newUserProperties.SamAccountName)}
                                    Default {}
                                }
                            }
                            else {
                                
                            }
                        }
                        catch {
                            Write-Warning "User Already Exist"
                            $i-=1
                        }
                        $i+=1
                    }
                }
            }
        }
    }
}

Add-ADGroupMember -Identity "Domain Admins" -Members "G_Responsable_Informatique"

Add-ADGroupMember -Identity "Print Operators" -Members $GrpE1
Add-ADGroupMember -Identity "Backup Operators" -Members $GrpE1
Add-ADGroupMember -Identity "Account Operators" -Members $GrpE1
Add-ADGroupMember -Identity "Server Operators" -Members $GrpE1

Add-ADGroupMember -Identity "Event Log Readers" -Members $GrpE2
Add-ADGroupMember -Identity "Performance Monitor Users" -Members $GrpE2
Add-ADGroupMember -Identity "Performance Log Users" -Members $GrpE2

Add-ADGroupMember -Identity $TotalR -Members "G_Responsable", "G_Direction"
Add-ADGroupMember -Identity $LectureR -Members "G_Informatique", "G_Comptabilité", "G_Marketing"
Add-ADGroupMember -Identity $RefusR -Members "G_Production"
