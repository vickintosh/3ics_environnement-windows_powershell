New-GPO -Name "AllowRunMenu" -Comment "Allow Run Menu"
Set-GPRegistryValue -Name "AllowRunMenu" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName '**del.NoRun' -Value 1 -Type DWord
New-GPLink -Name "AllowRunMenu" -Target "OU=Informatique,OU=Lyon,OU=Sites,DC=ESN,DC=dom" -LinkEnabled Yes
New-GPLink -Name "AllowRunMenu" -Target "OU=Informatique,OU=Paris,OU=Sites,DC=ESN,DC=dom" -LinkEnabled Yes


New-GPO -Name "DisallowRunMenu" -Comment "Disallow Run Menu"
Set-GPRegistryValue -Name "DisallowRunMenu" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName 'NoRun' -Value 1 -Type DWord
New-GPLink -Name "DisallowRunMenu" -Target "DC=ESN,DC=dom" -LinkEnabled Yes

# Set Password policy
Set-ADDefaultDomainPasswordPolicy -Identity ESN.dom -ComplexityEnabled $True -MaxPasswordAge 30.00:00:00 -MinPasswordAge 29.00:00:00 -MinPasswordLength 5 -PasswordHistoryCount 12
# End Password Policy for classic users

# Set Password Policy for IT
New-ADFineGrainedPasswordPolicy -Name "ITPasswordPolicy" -DisplayName "Specifics rules for IT teams password's" -ComplexityEnabled $true -Description "Specifics rules for IT teams password's" -MaxPasswordAge 30:00:00:00 -MinPasswordAge 29:00:00:00 -MinPasswordLength 9 -PasswordHistoryCount 12 -Precedence 100
Add-ADFineGrainedPasswordPolicySubject ITPasswordPolicy -Subjects "G_Informatique"
# End Password Policy for IT

<#
Get-GPO -All -Domain "ESN.dom"
Remove-GPO -Name "AllowRunMenu"
Remove-GPO -Name "DisallowRunMenu"
Remove-ADFineGrainedPasswordPolicy ITPasswordPolicy
#>


