#PingCastle

#==========STALE OBJECT==========#

#S-DC-SubnetMissing : Disable Ipv6 and add DC in declared subnets
$Subnet_list = ("172.31.0.0/24", "172.31.1.0/24")
foreach ($Subnet in $Subnet_list) {New-ADReplicationSubnet -Name "$Subnet" -Site "Default-First-Site-Name"}


#execute that on all DC
$Adapter_list = (Get-NetAdapter).Name
foreach ($Adapter in $Adapter_list) {Disable-NetAdapterBinding –InterfaceAlias “$Adapter” –ComponentID ms_tcpip6}


#S-ADRegistration : Prevents non-admin users from adding accounts to the domain
Set-ADDomain (Get-ADDomain).distinguishedname -Replace @{"ms-ds-MachineAccountQuota"="0"}


#S-PwdNeverExpires : Update accounts properties to remove "never-expiring passwords"
Get-ADUser -Filter "*" -Properties DistinguishedName, PasswordNeverExpires | Where-Object {$_.PasswordNeverExpires -eq "True"} | Where-Object {$_.Name -ne "Guest"} | ForEach-Object {Set-ADUser -Identity $_.DistinguishedName -PasswordNeverExpires:$false}


#==========PRIVILEGED ACCOUNTS==========#

#P-SchemaAdmin : Empty the schema admins group
Get-ADGroupMember -Identity 'Schema Admins' | ForEach-Object {Remove-ADGroupMember -Identity 'Schema Admins' -Members $_ -Confirm:$false}


#P-Delegated : Update admin account with the sensitive flag
Get-ADGroupMember -Identity "Domain Admins" | Set-ADUser -AccountNotDelegated $true


#(!! WIP !!) --> Miss "Certificate Operators" group ??
#P-ProtectedUsers : move all privileged account (account, not group !) in "Protected User"
#Get all Privileged account from "admin groups"
$AdminGroups=("Account Operators","Administrators","Backup Operators","Cert Publishers","DnsAdmins","Domain Admins","Enterprise Admins","Enterprise Key Admins","Key Admins","Print Operators","Replicator","Schema Admins","Server Operators")
[System.Collections.ArrayList]$List=@()
foreach ($Group in $AdminGroups) 
{
    Get-ADGroupMember -Identity "$Group" | ForEach-Object{$List.Add($_)} | Out-Null
}

while ($List.ObjectClass -contains "group")
{
    for($i=0; $i -lt $list.count; $i++)
    {
        if ($List[$i].ObjectClass -eq "group")
        {
            Get-ADGroupMember -Identity $List[$i].Name |ForEach-Object{$List.Add($_)} | Out-Null
            $List.Remove($List[$i])
        }
    }
}


#Empty the Protected User Group before add privileged account
Get-ADGroupMember -Identity 'Protected Users' | ForEach-Object {Remove-ADGroupMember -Identity 'Protected Users' -Members $_ -Confirm:$false}


#Add unique priviliged account
$Users = $List | select -Unique
Add-ADGroupMember -Identity "Protected Users" -Members $Users


#P-RecycleBin : Enable recyble bin
Enable-ADOptionalFeature -Identity 'Recycle Bin Feature' -Scope ForestOrConfigurationSet -Target (Get-ADDomain).DNSRoot -Confirm:$false


#==========TRUSTS==========#


#==========ANOMALIES==========#

#A-MinPwdLen : increase the password length to at least 8 characters
Set-ADDefaultDomainPasswordPolicy -Identity (Get-ADDomain).DNSRoot -MinPasswordLength 8 


#A-ReversiblePwd : Set all PSO ReversibleEncryption to false 
$PSO_List = (Get-ADFineGrainedPasswordPolicy -Filter "*").Name
foreach ($PSO in $PSO_List) {Set-ADFineGrainedPasswordPolicy -Identity "$PSO" -ReversibleEncryptionEnabled $false}
Set-ADDefaultDomainPasswordPolicy -Identity (Get-ADDomain).DNSRoot -ReversibleEncryptionEnabled $false


#A-DC-Spooler : Disable spooler service
Stop-Service -Name Spooler -Force
Set-Service -Name Spooler -StartupType Disabled


#(!! WIP !!) --> work with auditpol /get /category but not appear in GPO and impossible to find Advanced audit in pw 
#A-AuditDC : Fix the audit settings to collect key events
Set-GPRegistryValue -Name "Default Domain Controllers Policy" -Key "HKLM\System\CurrentControlSet\Control\Lsa" -ValueName 'SCENoApplyLegacyAuditPolicy' -Value 0 -Type DWord
Set-GPRegistryValue -Name "Default Domain Policy" -Key "HKLM\System\CurrentControlSet\Control\Lsa" -ValueName 'SCENoApplyLegacyAuditPolicy' -Value 0 -Type DWord

Remove-GPRegistryValue -Name "Default Domain Controllers Policy" -Key "HKLM\System\CurrentControlSet\Control\Lsa" -ValueName 'scenoapplylegacyauditpolicy'
Remove-GPRegistryValue -Name "Default Domain Policy" -Key "HKLM\System\CurrentControlSet\Control\Lsa" -ValueName 'scenoapplylegacyauditpolicy'


auditpol /set /category:"System","Logon/Logoff","Object Access","Privilege Use","Detailed Tracking","Policy Change","Account Management","DS Access","Account Logon" /failure:enable /success:enable
auditpol /get /category:*


#A-BackupMetadata : Plan AD backup 
Install-WindowsFeature Windows-Server-Backup

#list of disk(-Disk)/volume(-VolumePath) to backup
$Backup = Get-WBVolume -VolumePath "C:"
#Location of backup disk(-Disk) or volume(-VolumePath) not in same disk of the $volume
$BackupLocation = New-WBBackupTarget -VolumePath "E:"
#Store backup on network share
#$backupLocation = New-WBBackupTarget -NetworkPath \\backup.esn.dom\Partage -Credential (Get-Credential)
$Policy = New-WBPolicy

Add-WBBackupTarget -Policy $Policy -Target $BackupLocation
Add-WBVolume -Policy $Policy -Volume $Backup
Add-WBBareMetalRecovery -Policy $Policy

Add-WBSystemState -Policy $Policy

Set-WBSchedule -Policy $Policy -Schedule 22:00
Set-WBPolicy -Policy $Policy -Confirm:$false -Force

#First backup before the Schedule
#Start-WBBackup -Policy $Policy


#(!! WIP !!) --> Just for pingcastle points, not really work
#A-LAPS-Not-Installed : Install LAPS (Local Administrator Password Solution)
$URLs = @('https://download.microsoft.com/download/C/7/A/C7AAD914-A8A6-4904-88A1-29E657445D03/LAPS.x64.msi','https://download.microsoft.com/download/C/7/A/C7AAD914-A8A6-4904-88A1-29E657445D03/LAPS.x86.msi')
$TempFolder = "C:\Script\laps"
New-Item -Path $TempFolder -ItemType Directory -Force
$URLs | foreach-object {

    $fileName = Split-Path $_ -Leaf
    $DestinationPath = Join-Path $TempFolder -ChildPath $fileName
    Invoke-WebRequest -Uri $_ -OutFile $DestinationPath
}

Start-Process msiexec -ArgumentList "/i $TempFolder\LAPS.x64.msi ADDLOCAL=Management.UI,Management.PS,Management.ADMX /quiet /norestart" -Wait -NoNewWindow -PassThru

#Miss real instalation here

Import-Module AdmPwd.PS
Update-AdmPwdADSchema


#=========Other=========#
#Protect OU from accidental deletion
$OU_list = (Get-ADOrganizationalUnit -Filter "*" -SearchBase (Get-ADDomain).DistinguishedName).DistinguishedName
foreach ($OU in $OU_list) {Set-ADOrganizationalUnit -Identity "$OU" -ProtectedFromAccidentalDeletion $false}

