﻿$Scope=@{
    NameDomain = "ESN.dom"
    'CliLyon'=@{
        Network = "172.31.0.0"
        StartRange = "172.31.0.128"
        EndRange = "172.31.0.253"
        Mask = "255.255.255.0"
        Gateway = "172.31.0.254" 
        AdresseDNS = "172.31.0.1"
    }
    'CliParis'=@{
        Network = "172.31.1.0"
        StartRange = "172.31.1.128"
        EndRange = "172.31.1.253"
        Mask = "255.255.255.0"
        Gateway = "172.31.1.254" 
        AdresseDNS = "172.31.1.1"
    }
}

foreach ($item in $Scope.Keys) {
    Add-DHCPServerv4Scope -Name $Scope.$item -StartRange $Scope.$item.StartRange -EndRange $Scope.$item.EndRange -SubnetMask $Scope.$item.Mask -State Active
    Set-DHCPServerv4OptionValue -ScopeID $Scope.$item.Network  -DnsDomain $Scope.NameDomain -DnsServer $Scope.$item.AdresseDNS -Router $Scope.$item.Gateway
    Add-DhcpServerInDC -DnsName $Scope.NameDomain -IpAddress $Scope.$item.AdresseDNS
}

Get-DhcpServerv4Scope
Restart-service dhcpserver