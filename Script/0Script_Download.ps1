﻿$GitFolder = "CD"
$NewFolder = "C:\Script\"
$DLFile = "script.zip"
$ZipPath = "3ics_environnement-windows_powershell-main-Script-"+$GitFolder+"/Script/"+$GitFolder
$URL = "https://gitlab.com/vickintosh/3ics_environnement-windows_powershell/-/archive/main/3ics_environnement-windows_powershell-main.zip?path=Script/"+$GitFolder
$DLPath = ("$NewFolder"+"$DLFile")
$UnzipPath = ("$NewFolder"+"$ZipPath")

New-Item -Path $NewFolder -ItemType Directory
Invoke-WebRequest -Uri $URL -OutFile $DLPath
Expand-Archive -LiteralPath $DLPath -DestinationPath $NewFolder
Copy-Item -Path $UnzipPath -Destination $NewFolder -recurse -Force
Remove-Item ("$NewFolder"+"*") -Recurse -Exclude $GitFolder
