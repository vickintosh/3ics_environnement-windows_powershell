﻿$IPAddress = "172.31.75.1"
$Prefix = "16"
$Gateway = "172.31.255.255"
$IPAddressDNS = "172.31.1.1"

New-NetIPAddress -IPAddress $IPAddress -PrefixLength $Prefix -InterfaceIndex (Get-NetAdapter).ifIndex -DefaultGateway $Gateway
Set-DnsClientServerAddress -InterfaceIndex (Get-NetAdapter).ifIndex -ServerAddresses ($IPAddressDNS)